
import java.io.*;

class program10 {
	public static void main(String[] arg) throws IOException {

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the number of rows ");
		int row = Integer.parseInt(br.readLine());

		for (int i = 0; i < row; i++) {

			for (int j = 0; j <= i; j++) {

				if (i == 0) {
					System.out.print("1 ");
				} else if (i == 1) {
					if (j == 0) {
						System.out.print("b");
					} else {
						System.out.print("c");
					}
					if (j < i) {
						System.out.print(" ");
					}
				} else {
					System.out.print(j + 4 + " ");
				}
			}

			System.out.println();
		}
	}
}
