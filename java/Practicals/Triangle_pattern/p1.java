
import java.io.*;
class program1{
	public static void main(String []arg) throws IOException{

		BufferedReader br = new BufferedReader ( new InputStreamReader(System.in));
		System.out.println("Enter the number of rows ");
		int row = Integer.parseInt(br.readLine());

		int num = row*row;
		for(int i = 1; i<=row ; i++){
			for(int j=1;j<=i;j++){
				System.out.print(num +" ");
			}
			System.out.println();
		}
	}
}


