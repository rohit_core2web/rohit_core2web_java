
import java.io.*;

class program9 {
	public static void main(String[] arg) throws IOException {

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the number of rows ");
		int row = Integer.parseInt(br.readLine());
		for (int i = 1; i <= 3; i++) {

			System.out.print("4 ");

			for (int j = 1; j <= i; j++) {

                        char ch = (char) ('a' + (j - 1));

				if (i == 2 && j == 2) {
					ch = 'b';
				}

				System.out.print(ch + " ");
				
			}

			System.out.println();
		}
	}
}
