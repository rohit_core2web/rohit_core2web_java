
class program12 {
    public static void main(String[] args) {

        int num1 = 10;
        int num2 = 5;
        int result = num1 + num2;

        boolean isJavaFun = true;
        boolean isJavaEasy = false;
        boolean isJavaInteresting = isJavaFun && !isJavaEasy;
        boolean isGreater = (num1 > num2);

        System.out.println("Arithmetic Operator:");
        System.out.println(result);

        System.out.println("\nLogical Operator:");
        System.out.println(isJavaInteresting);

        System.out.println("\nComparison Operator:");
        System.out.println(isGreater);
    }
}
