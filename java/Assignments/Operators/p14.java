
class program14 {
    public static void main(String[] args) {
        int x = 14;
        int y = 22;

        int result1 = ++x + y++;
        System.out.println("output 1: " + result1);

        x = 14;
        y = 22;

        int result2 = x++ + ++y + ++x + ++x;
        System.out.println("output 2: " + result2);

        x = 14;
        y = 22;

        int result3 = y++ + ++x + ++x;
        System.out.println("output 3: " + result3);
    }
}
