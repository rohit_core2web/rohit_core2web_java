
class program11 {
    public static void main(String[] args) {

        int number = 10;
        int rightShiftResult = number >> 2;
        int leftShiftResult = number << 2;

        System.out.println(number);
        System.out.println(rightShiftResult);
        System.out.println(leftShiftResult);
    }
}
