
class program5 {
    public static void main(String[] args) {
        int num1 = 5;
        int num2 = 3;

        int bitwiseAnd = num1 & num2;
        int bitwiseOr = num1 | num2;
        int bitwiseXor = num1 ^ num2;
        int leftShift = num1 << 1;
        int rightShift = num1 >> 1;

        System.out.println(num1 + " & " + num2 + " = " + bitwiseAnd);
        System.out.println(num1 + " | " + num2 + " = " + bitwiseOr);
        System.out.println(num1 + " ^ " + num2 + " = " + bitwiseXor);
        System.out.println(num1 + " << 1 = " + leftShift);
        System.out.println(num1 + " >> 1 = " + rightShift);
    }
}
