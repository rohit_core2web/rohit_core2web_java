
class program13 {
    public static void main(String[] args) {
        int x = 19;

        int result1 = x++ + x++;
        System.out.println("output 1: " + result1);

        x = 19;

        int result2 = ++x + x++ + ++x;
        System.out.println("output 2: " + result2);
    }
}
