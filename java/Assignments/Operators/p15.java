class program15 {
    public static void main(String[] args) {
        int x = 0;

        int result1 = ++x + ++x + ++x + ++x;
        System.out.println("output 1: " + result1);

        int result2 = x++ + x++ + x++ + x++;
        System.out.println("output 2: " + result2);
    }
}
