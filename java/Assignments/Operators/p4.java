
class program4 {
	public static void main(String[] args) {
		boolean firstValue = true;
		boolean secondValue = false;

		boolean logicalAnd = firstValue && secondValue;
		boolean logicalOr = firstValue || secondValue;
		boolean logicalNotFirst = !firstValue;
		boolean logicalNotSecond = !secondValue;

		System.out.println(firstValue + " && " + secondValue + " = " + logicalAnd);
		System.out.println(firstValue + " || " + secondValue + " = " + logicalOr);
		System.out.println("!" + firstValue + " = " + logicalNotFirst);
		System.out.println("!" + secondValue + " = " + logicalNotSecond);
	}
}
