class Program3 {
    public static void main(String[] args) {

        double pi = 3.14159;
        double triangleAngle = 45.0;

        double base = 5.0;
        double height = 8.0;
        double triangleArea = 0.5 * base * height;

        int circleDegree = 360;

        System.out.println("Value of Pi: " + pi);
        System.out.println("Angle of the triangle: " + triangleAngle + " degrees");
        System.out.println("Area of the triangle: " + triangleArea);
        System.out.println("Highest degree of angle in a circle: " + circleDegree);
    }
}
