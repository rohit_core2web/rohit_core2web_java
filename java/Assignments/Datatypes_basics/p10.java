class program10 {
    public static void main(String[] args) {

        double boxOfficeCollection = 150000000.00;

        double imdbRating = 8.5;

        System.out.println("Box Office Collection: $" + boxOfficeCollection);
        System.out.println("IMDb Rating: " + imdbRating);
    }
}
