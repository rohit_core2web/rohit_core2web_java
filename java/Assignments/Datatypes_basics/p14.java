class program14 {
    public static void main(String[] args) {
        int[] asciiValues = { 67, 79, 82, 69, 50, 87, 69, 66 };

        System.out.print("Characters: ");
        for (int value : asciiValues) {
            System.out.print((char) value);
        }
        System.out.println();
    }
}
