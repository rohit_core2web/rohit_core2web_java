class program11 {
    public static void main(String[] args) {

        double gravity = 9.81;
        char gravityLetter = 'g';
        System.out.println("Value of Gravity: " + gravity + " m/s^2");
        System.out.println("Letter for Acceleration due to Gravity: " + gravityLetter);
    }
}
