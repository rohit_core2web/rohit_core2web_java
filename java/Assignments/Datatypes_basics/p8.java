class program8 {
    public static void main(String[] args) {

        int englishMarks = 70;
        int mathMarks = 98;
        int scienceMarks = 75;

        int totalMarks = englishMarks + mathMarks + scienceMarks;
        double percentage = (double) totalMarks / 3;

        char grade;
        if (percentage >= 90) {
            grade = 'A';
        } else if (percentage >= 80) {
            grade = 'B';
        } else if (percentage >= 70) {
            grade = 'C';
        } else if (percentage >= 60) {
            grade = 'D';
        } else if (percentage >= 50) {
            grade = 'E';
        } else {
            grade = 'F';
        }

        System.out.println(totalMarks);
        System.out.println(percentage + "%");
        System.out.println(grade);
    }
}
