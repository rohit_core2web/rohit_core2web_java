class program15 {
    public static void main(String[] args) {
        int age = 25;
        double height = 5.8;
        String name = "John";
        boolean isMale = true;
        char grade = 'A';

        System.out.println("Name: " + name);
        System.out.println("Age: " + age);
        System.out.println("Height: " + height + " feet");
        System.out.println("Is Male: " + isMale);
        System.out.println("Grade: " + grade);
    }
}
