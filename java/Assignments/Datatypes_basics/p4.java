class program4 {
    public static void main(String[] args) {

        int employeeCount = 100;

        double travelExpenses = 50000.0;

        System.out.println("Employee count of the company: " + employeeCount);
        System.out.println("Total expenses of the company on travel: $" + travelExpenses);
    }
}
