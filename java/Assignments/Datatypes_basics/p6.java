class program6 {
    public static void main(String[] args) {

        int d = 25, m = 2, y = 2024;

        int secMin = 60, minHour = 60, hourDay = 24, dayMonth = 30, monthYear = 12;

        int daySec = secMin * minHour * hourDay;
        int monthSec = daySec * dayMonth;
        int yearSec = monthSec * monthYear;

        System.out.println("Date: " + d + "\nMonth: " + m + "\nYear: " + y);
        System.out.println("Total seconds in a day: " + daySec);
        System.out.println("Total seconds in a month: " + monthSec);
        System.out.println("Total seconds in a year: " + yearSec);
    }
}
